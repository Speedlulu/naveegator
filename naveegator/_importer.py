#!/usr/bin/env python3


import importlib
import re
from os.path import basename, dirname
from urllib.parse import urlparse


__all__ = ('find_site_klass_from_url', )


def find_site_klass_from_url(url):
    # get rid of the scheme and then of the www.
    parsed_url = urlparse(url)
    if parsed_url.scheme:
        clean_url = parsed_url.netloc
    else:
        clean_url = parsed_url.path
    clean_url = clean_url.lstrip('w.')

    sites_package = importlib.import_module(
        '.sites',
        basename(dirname(__file__)),
    )

    for site_class in sites_package.__all__:
        klass = getattr(sites_package, site_class)
        for url_regex in klass.URLS:
            if re.match(url_regex, clean_url):
                return klass
    else:
        raise RuntimeError('No site defined for url : %s' % url)
