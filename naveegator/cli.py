#!/usr/bin/env python3


import argparse
import sys


if __package__ is None and not hasattr(sys, 'frozen'):
    # direct call of cli.py
    import os.path
    path = os.path.realpath(os.path.abspath(__file__))
    sys.path.insert(0, os.path.dirname(os.path.dirname(path)))


from naveegator.helpers import (
    number_of_search_results,
)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('url', help='Url to do action on')

    subparsers = parser.add_subparsers()

    number_results_parser = subparsers.add_parser(
        'number-results',
        help='Fetch the number of results for keyword',
    )

    number_results_parser.set_defaults(
        func=number_of_search_results,
        display='%d results',
    )

    number_results_parser.add_argument(
        'keyword',
        help='Keyword to search',
    )

    args = vars(parser.parse_args())

    func = args.pop('func', None)
    display = args.pop('display', None)

    if func is None:
        parser.error('Need to specifiy an action')

    try:
        results = func(**args)
    except Exception as e:
        print(e)
        return -1

    if display is not None:
        print(display % results)

    return 0


if __name__ == '__main__':
    sys.exit(main())
