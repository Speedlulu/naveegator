#!/usr/bin/env python3


from ._importer import find_site_klass_from_url


__all__ = ('number_of_search_results', )


def number_of_search_results(url, keyword):
    site_klass = find_site_klass_from_url(url)

    site = site_klass(url)

    return site.search(keyword)
