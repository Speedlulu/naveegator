#!/usr/bin/env python3


"""
Import all classes present in __all__ of all python files in this
folder, except those in BLACKLIST
"""

import importlib
import inspect
import sys
from pathlib import Path


__all__ = []


BLACKLIST = ('__init__.py', 'site.py', )
CURRENT_DIRECTORY = Path(__file__).parent

modules = [
    path.stem
    for path in CURRENT_DIRECTORY.glob('*.py')
    if path.is_file() and path.name not in BLACKLIST
]

for module in modules:
    _m = importlib.import_module('.%s' % module, __name__)
    for _class in dict(inspect.getmembers(_m)).get('__all__', []):
        setattr(sys.modules[__name__], _class, getattr(_m, _class))
        __all__.append(_class)
