#!/usr/bin/env python3


from .site import HtmlSite


__all__ = ('JdHk', )


class JdHk(HtmlSite):
    URLS = (
        'jd.hk',
        'search.jd.hk',
    )

    def search(self, keyword):
        if not self._base_url.endswith('search.jd.hk'):
            self.set_base_url('search.jd.hk')

        result = self.get(
            'Search',
            params={
                'keyword': keyword,
                'enc': 'utf-8',
            },
        )

        html_tree = self.parse(result.text)

        return len(html_tree.xpath('//li[@class="gl-item"]'))
