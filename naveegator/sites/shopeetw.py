#!/usr/bin/env python3


from .site import Site


__all__ = ('ShopeeTw', )


class ShopeeTw(Site):
    URLS = (
        'shopee.tw',
    )

    def search(self, keyword):
        result = self.get(
            'api/v4/search/search_items',
            params={
                'keyword': keyword,
            },
        )

        return result.json()['total_count']
