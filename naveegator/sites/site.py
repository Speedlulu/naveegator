#!/usr/bin/env python3


import requests
from lxml import etree
from requests.exceptions import SSLError


__all__ = ('Site', 'HtmlSite', )


class Site():
    URLS = None

    def __init__(self, url):
        self.session = requests.Session()
        self._base_url = None
        self.set_base_url(url)

    def set_base_url(self, url):
        if not url.startswith('http'):
            # guess http or https
            url = url.strip('/') + '/'
            try:
                self.session.get('https://' + url)
            except SSLError:
                self._base_url = 'http://' + url
            else:
                self._base_url = 'https://' + url
        else:
            self._base_url = url

    def get(self, endpoint='/', *args, **kwargs):
        return self.session.get(
            self._base_url + endpoint.lstrip('/'),
            *args,
            **kwargs
        )

    def post(self, endpoint='/', *args, **kwargs):
        return self.session.post(
            self._base_url + endpoint.lstrip('/'),
            *args,
            **kwargs
        )

    def search(self, keyword):
        raise NotImplementedError(
            'Need to implement search for class: %s' % self.__class__.__name__
        )


class HtmlSite(Site):
    def __init__(self, url):
        self.parser = etree.HTMLParser()
        super(HtmlSite, self).__init__(url)

    def parse(self, html_string):
        return etree.fromstring(html_string, parser=self.parser)
