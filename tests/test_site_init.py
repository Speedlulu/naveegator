import importlib
import shutil
import sys
import unittest
from pathlib import Path


CURRENT_DIR = Path(__file__).parent
TEST_DATA_PATH = CURRENT_DIR.joinpath('data/site_init')
SITES_PATH = CURRENT_DIR.parent.joinpath('naveegator/sites')


class ModuleInitTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # copy fake sites
        for path in TEST_DATA_PATH.glob('*.py'):
            shutil.copyfile(
                path,
                SITES_PATH.joinpath(path.name)
            )
        # force reload to take new files into account
        if sys.modules.get('naveegator.sites') is not None:
            importlib.reload(sys.modules['naveegator.sites'])
        else:
            importlib.import_module('naveegator.sites')

    def test_file_imported(self):
        self.assertIsNotNone(
            sys.modules.get('naveegator.sites.testsite1')
        )
        self.assertIsNotNone(
            sys.modules.get('naveegator.sites.testsite2')
        )

    def test_init_all(self):
        self.assertIn(
            'TestClass1',
            sys.modules['naveegator.sites'].__all__
        )
        self.assertIn(
            'TestClass2',
            sys.modules['naveegator.sites'].__all__
        )
        self.assertIn(
            'TestClass3',
            sys.modules['naveegator.sites'].__all__
        )
        self.assertNotIn(
            'TestClass4',
            sys.modules['naveegator.sites'].__all__
        )

    def test_init_class(self):
        import naveegator.sites
        naveegator.sites.TestClass3()
        self.assertTrue(True)

    @classmethod
    def tearDownClass(cls):
        # delete fake sites
        for path in TEST_DATA_PATH.glob('*.py'):
            SITES_PATH.joinpath(path.name).unlink()
        # force reload to get rid of deleted files
        importlib.reload(sys.modules['naveegator.sites'])
