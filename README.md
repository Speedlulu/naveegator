# Naveegator

Naveegator is a simple tool to scrap websites.

## Dependencies

* Python >= 3.7
* requests >= 2.25.1
* lxml >= 4.6.3

## Sites

You can add your own sites.
You just have to create a new python file in the *sites/* folder, and add your class (preferably subclassing the `Site` or `HTMLSite` classes found in
*sites/site.py*).
Two sites come preloaded and can be used as an example.

## Usage

### From shell

After installing naveegator you can use the bundled script :

```bash
$ naveegator -h
```

### From code

You can use the helper functions provided in *helpers.py*.

```python
from naveegator.helpers import number_of_search_results

number_of_search_results('jd.hk', 'banana')
```

Or interact with the sites directly.

```python
from naveegator.sites import JdHk

site = JdHk('jd.hk')

site.search('banana')
```

## Installation

From source.

```bash
$ git clone git@gitlab.com:Speedlulu/naveegator.git naveegator
$ cd naveegator
$ python3 -m venv venv
$ source venv/bin/activate
$ python3 -m pip install -e .
```

You might want to run the tests.

```bash
$ python3 -m unittest -v
```
