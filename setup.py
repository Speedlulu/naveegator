from setuptools import setup, find_packages

setup(
    name="naveegator",
    version='0.1',
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'naveegator = naveegator.cli:main',
        ],
    },
    author='Lucas Ficheux',
    author_email='lucas.ficheux@gmail.com',
    url='https://gitlab.com/Speedlulu/naveegator',
    python_requires='>=3.7',
    install_requires=[
        'requests>=2.25.1',
        'lxml>=4.6.3',
    ]
)
